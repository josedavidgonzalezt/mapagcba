import React,{Fragment} from 'react';
import {Map,TileLayer} from 'react-leaflet';
import CapasMapas from './CapasMapas';
import Seleccion from './Seleccion';


const Mapa = () => {
 

    return ( 
        <Fragment>
            <Map center={[-34.614786,-58.4554439]} zoom={12}>
                <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                
                <Seleccion/>
                <CapasMapas/>
            
            </Map>
        </Fragment>
     );
}
 
export default Mapa;