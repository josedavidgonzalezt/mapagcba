import React, {Fragment,useState, useEffect} from 'react';
import {Marker, Popup} from 'react-leaflet';
import {Icon} from 'leaflet';
/* import HeatmapLayer from 'react-leaflet-heatmap-layer'; */
import reciclar from './/imgMarkers/reciclar.png';
    
const CampanasVerdes = () => {
    //state para actualizar el mostrar o ocultar las campanas
    const [mostrar, setMostrar]= useState(false);

    //state de campanas
    const [campanas, setCampanas] =useState([]);

    //state del popup
    const [popup, setPopUp] =useState(null);

    //funcion para mostrar y ocultar campanas
    const mostrarOcultar=()=>{
        if(mostrar){
            setMostrar(false);
        }
        else if(!mostrar){
            setMostrar(true);
        }
    }

    //icono de campanas verdes
    const campanasmarker = new Icon({
        iconUrl: require('./imgMarkers/reciclar.png'),
        iconSize: 35
    });

    //funcion para consultar la api
    const consutarApi=async()=>{
        const url="https://cors-anywhere.herokuapp.com/http://cdn.buenosaires.gob.ar/datosabiertos/datasets/campanas-verdes/campanas-verdes.geojson";
        const api= await fetch(url);
        const resultado = await api.json();
        /* console.log(resultado.features); */
        setCampanas(resultado.features);
    }
    
   
    useEffect(()=>{
        consutarApi();
    },[]);

    // para acceder a cada elemento del arreglo con el valor de su posición y asiganarle un id
    for(let i = 0; i < campanas.length ; i ++){
        campanas[i].id =i;
      }

    return ( 
        <Fragment>
            <label>
                <img className="img-ico" src={reciclar} alt="icono marker"/>
                Campanas Verdes <input type="checkbox" defaultChecked={mostrar} onClick={()=>mostrarOcultar()}/>
            </label>
            <p className="p-cantidad">Total: {campanas.length}</p>

            {mostrar? campanas.map(campana=>(
                <Marker
                    icon={campanasmarker}
                    key={campana.id}
                    onClick={()=>setPopUp(campana)}
                    position={[campana.geometry.coordinates[1],
                                campana.geometry.coordinates[0]]}
                />
            )):null}

            { popup && 
                <Popup
                    key={popup.id}
                    onClose={()=>setPopUp(null)}
                    position={[popup.geometry.coordinates[1],
                                popup.geometry.coordinates[0]]}
                >
                    <div className="mypopup">
                        <p><strong>Modelo:</strong> {popup.properties.MODELO}</p>
                        <p><strong>Ubicación:</strong> calle {popup.properties.CALLE} al {popup.properties.ALTURA}</p>
                    </div>
                </Popup>
            }

            
               {/*  <HeatmapLayer

                    points={campanas.map(campana=>([campana.geometry.coordinates[1],
                                campana.geometry.coordinates[0]]))}
                    fitBoundsOnLoad
                    fitBoundsOnUpdate
                    longitudeExtractor={m => m[1]}
                    latitudeExtractor={m => m[0]}
                    intensityExtractor={m => parseFloat(m[2])}
                /> */}
        
            

        </Fragment>
     );
}
 
export default CampanasVerdes;