import React, {Fragment,useState} from 'react';
import {Marker,Popup} from 'react-leaflet';
import {Icon} from 'leaflet';
import * as puntosverdes from '../../data/puntosverdes.json';
import campanaverde from './imgMarkers/ubicacion2.png';
import puntoverde from './imgMarkers/ubicacion.png';

const PuntosVerdes = () => {
    //state para mostrar y ocultar el pop up
    const [popupverde,setPopUpVerde ]=useState(null);

    //states para puntos verdees y campanas
    const [mostrar, actualizarMostrar]=useState(false);
    const [mostrar2, actualizarMostrar2]=useState(false);

    //funcion que muestra y oculta los puntos verdes
    const ocultarMostrar=()=>{
        if(mostrar){
            actualizarMostrar(false);
        }
        else if(!mostrar){
            actualizarMostrar(true);
        }
    }

      //funcion que muestra y oculta los las campanas
    const ocultarMostrar2=()=>{
        
        if(mostrar2){
            actualizarMostrar2(false);
        }
        else if(!mostrar2){
            actualizarMostrar2(true);
        }
    }

    //iconos de los markers punto verde y campana
    const markerVerde = new Icon({
        
          iconUrl: require('./imgMarkers/ubicacion.png'),
          iconSize:35
        
      });

    const markerVerde2 = new Icon({
        
          iconUrl: require('./imgMarkers/ubicacion2.png'),
          iconSize:35
        
      });
      
    
     
    return ( 
        <Fragment>
  
            <label>
                <img className="img-ico" src={puntoverde} alt="icono marker"/>
                Puntos Verdes <input type="checkbox"  defaultChecked={mostrar} onClick={()=>ocultarMostrar()}/>
            </label>
            <p className="p-cantidad">Total: {puntosverdes.properties.filter(filtro=>filtro.tipo_norma==='punto_verde_con_atencion').length}</p>
            
            <label>
                <img className="img-ico" src={campanaverde} alt="icono marker"/>
                P. Verdes Especiales <input type="checkbox"  defaultChecked={mostrar2} onClick={()=>ocultarMostrar2()}/>
             </label>
             <p className="p-cantidad">Total: {puntosverdes.properties.filter(filtro=>filtro.tipo_norma==='punto_verde_especial').length}</p>

            { mostrar ? puntosverdes.properties.filter(filtro=>filtro.tipo_norma==='punto_verde_con_atencion').map(puntoverde=>(
                <Marker
                    icon={markerVerde}
                    key={puntoverde.id}
                    position={[puntoverde.lat,
                                puntoverde.long]}
                    onclick={()=>setPopUpVerde(puntoverde)}
                />
            
            )) :null}
            { mostrar2 ? puntosverdes.properties.filter(filtro=>filtro.tipo_norma==='punto_verde_especial').map(puntoverde=>(
                <Marker
                    icon={markerVerde2}
                    key={puntoverde.id}
                    position={[puntoverde.lat,
                                puntoverde.long]}
                    onclick={()=>setPopUpVerde(puntoverde)}
                />
            
            )) :null}
            {popupverde &&
                <Popup 
                    position={[popupverde.lat,
                                popupverde.long]}
                    onClose={()=>{setPopUpVerde(null);}}
                    >
                    
                    <div className="mypopup">
                        <p><strong>Plaza:</strong> {popupverde.plaza}</p>
                        <p><strong>Cooperativa:</strong> {popupverde.cooperativa}</p>
                        <p><strong>Horario:</strong> {popupverde.dias_y_horario}</p>
                        <p><strong>Tipo:</strong> {popupverde.tipo}</p>
                        <p>{popupverde.comuna}</p>
                        <p>{popupverde.direccion}</p>
                        <p>{popupverde.barrio}</p>
                        
                    </div>
                </Popup>
            }
            
        </Fragment>
    );
}
 
export default PuntosVerdes;