import React,{Fragment,useState} from 'react';
import * as wifis from '../../data/datapuntoswifi.json';
import iconwifi from './imgMarkers/señal.png'

import {Marker,Popup} from 'react-leaflet';
import {Icon} from 'leaflet';


const PuntosWifi = () => {
    const [mostrar, setMostrar] = useState(false);
    const [popupwifi, setPopupWifi] = useState(null);

    const ocultarMostrar=()=>{
            if (!mostrar){
                setMostrar(true);
            }
            else if(mostrar){
                setMostrar(false)
            }
    }
    
    const wifimarker = new Icon({
        iconUrl: require('./imgMarkers/señal.png'),
        iconSize: 35
    })

    return ( 
        <Fragment>
            <label>
                <img className="img-ico" src={iconwifi} alt="icono marker"/>
                Wifi Público <input type="checkbox"  defaultChecked={mostrar} onClick={()=>ocultarMostrar()}/>
            </label>
            <p className="p-cantidad">Total: {wifis.default.wifi.length}</p>

            
            {mostrar? wifis.default.wifi.map(puntowifi=>(
                <Marker
                    key={puntowifi.id}
                    icon={wifimarker}
                    position={[puntowifi.lat,
                                puntowifi.long]}

                    onClick={()=>setPopupWifi(puntowifi)}
                />
            )) :null}
            {   popupwifi &&
                <Popup
                position={[popupwifi.lat,
                            popupwifi.long]}
                    onClose={()=>setPopupWifi(null)}
                >
                    <div className="mypopup">
                        <p><strong>Nombre:</strong> {popupwifi.nombre}</p>
                        <p><strong>Tipo:</strong> {popupwifi.tipo}</p>
                        <p><strong>Barrio:</strong> {popupwifi.barrio}</p>
                        <p><strong>Dirección:</strong> {popupwifi.calle_nombre} {popupwifi.calle_altura}</p>
                        <p><strong>Estado:</strong> {popupwifi.estado}</p>
                    </div>
                </Popup>

            }
        </Fragment>
     );
}
 
export default PuntosWifi;