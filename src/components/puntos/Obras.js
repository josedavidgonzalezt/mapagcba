import React, {Fragment, useState, useEffect} from 'react';
import {Popup, Marker} from 'react-leaflet';
import obraslabel from './imgMarkers/construccion.png';
import {Icon} from 'leaflet';

const Obras = () => {
       //state para las obras
       const [obras, setObras]=useState(false);

       //state para los popups
       const [popup, setPopup]=useState(null);

       //state para la api
       const [obrasdataset, setObrasDataSet] = useState([]);

    
       //funcion que muestra y oculta las obras
       const mostrarObras =()=>{
           if (!obras){
               setObras(true);
           }
           else if(obras){
               setObras(false)
           }
       }

       //consultamos la api
        const consultarApi=async()=>{
                const url="https://cors-anywhere.herokuapp.com/http://cdn.buenosaires.gob.ar/datosabiertos/datasets/obras-registradas/obras-registradas-junsep2019.geojson";
                const api = await fetch(url);
                const resultado = await api.json();
                setObrasDataSet(resultado.features);
                
            }
            
        useEffect(() => {
            consultarApi();
       }, [])
       /* console.log(obrasdataset); */
   
   //icono para obras

   const obrasmarker= new Icon({
    iconUrl: require('./imgMarkers/construccion.png'),
    iconSize:35
   });
   
      
      
       return ( 
           <Fragment>
               <label>
                    <img className="img-ico" src={obraslabel} alt="icono marker"/>
                   Obras (2019) <input id="#id" type="checkbox" defaultChecked={obras} onClick={()=>mostrarObras()}/>
                </label>
                <p className="p-cantidad">Total: {obrasdataset.length}</p>
               {obras 
                ? obrasdataset.map(obra=>(
                    <Marker
                        key={obra.geometry.coordinates[0]}
                        icon={obrasmarker}
                        onClick={()=>setPopup(obra)}
                        position={[obra.geometry.coordinates[1],
                                    obra.geometry.coordinates[0]]}
                        
                    /> 
                )) 
                :null}

                {popup &&
                    <Popup
                        position={[popup.geometry.coordinates[1],
                                    popup.geometry.coordinates[0]]}
                        onClose={()=>setPopup(null)}
                    >
                        <div className="mypopup">
                            <p><strong>Tipo de Obra:</strong> {popup.properties["TIPO DE OB"]}</p>
                            <p><strong>Nro Expediente:</strong> {popup.properties["Nº EXPEDI"]}</p>
                            <p><strong>Comuna:</strong> {popup.properties.COMUNA}</p>
                            <p><strong>Dirección:</strong> {popup.properties.DIRECCION}</p>
                        </div>
                    </Popup>
                }
               
   
           </Fragment>
        );
}
 
export default Obras;