import React, {Fragment,useEffect,useState} from 'react';
import {Marker,Popup} from 'react-leaflet';
import {Icon} from 'leaflet';
import casa from './imgMarkers/casa.png';
import viviendaico from './imgMarkers/vivienda.png';


const Edificios = () => {
    //state para guardar la consulta de la api
    const [edificios, setEdificios]= useState([]);
    //state para mostrar/ocultar los edificios
    const [mostrar, setMostrar] = useState(false);
    const [mostrar2, setMostrar2] = useState(false);
    
    //state para el pop up
    const [popup, setPopUp] =useState(null);

    //funcion que consulta la api
    const consultarApi=async()=>{
        const url="https://cors-anywhere.herokuapp.com/http://cdn.buenosaires.gob.ar/datosabiertos/datasets/relevamiento-usos-del-suelo/relevamiento-usos-suelo-2017.geojson";
        const api = await fetch(url);
        const resultado = await api.json();
        setEdificios(resultado.features);
        
    }

    useEffect(()=>{
        consultarApi();
    },[])

    for (let i =0; i< edificios.length; i++){
        edificios[i].properties.id=i;
    }

    //funcion que muestra y oculta
    const mostrarOcultar=()=>{
        if(mostrar){
            setMostrar(false);
        }
        else if(!mostrar){
            setMostrar(true);
        }

    }
    
    const mostrarOcultar2=()=>{
        if(mostrar2){
            setMostrar2(false);
        }
        else if(!mostrar2){
            setMostrar2(true);
        }

    }

    //incono de edificios 

    const edi10marker= new Icon({
        iconUrl: require('./imgMarkers/casa.png'),
        iconSize:20
    });
    
    const viviendamarker= new Icon({
        iconUrl: require('./imgMarkers/vivienda.png'),
        iconSize:20
    });

    // variable que filtra y almacena todos los edificios que tengan mas de 10 pisos

    const totaledif = edificios.filter(filtro=>filtro.properties.PISOS_16==='11' || filtro.properties.PISOS_16=== '12' || filtro.properties.PISOS_16==='13' || filtro.properties.PISOS_16=== '14' || filtro.properties.PISOS_16==='15' || filtro.properties.PISOS_16==='16'|| filtro.properties.PISOS_16==='17' ||filtro.properties.PISOS_16=== '18' || filtro.properties.PISOS_16==='19'|| filtro.properties.PISOS_16=== '20' || filtro.properties.PISOS_16==='21' || filtro.properties.PISOS_16=== '22' || filtro.properties.PISOS_16==='23' || filtro.properties.PISOS_16==='24' || filtro.properties.PISOS_16==='25' || filtro.properties.PISOS_16==='26'||filtro.properties.PISOS_16=== '27' || filtro.properties.PISOS_16==='28' || filtro.properties.PISOS_16==='29' || filtro.properties.PISOS_16=== '30' || filtro.properties.PISOS_16==='31'|| filtro.properties.PISOS_16=== '32' || filtro.properties.PISOS_16=== '33' || filtro.properties.PISOS_16=== '34' || filtro.properties.PISOS_16=== '35' ||  filtro.properties.PISOS_16==='36' || filtro.properties.PISOS_16==='37' ||  filtro.properties.PISOS_16==='38' || filtro.properties.PISOS_16=== '39' ||  filtro.properties.PISOS_16==='40' || filtro.properties.PISOS_16=== '41' || filtro.properties.PISOS_16==='42' || filtro.properties.PISOS_16==='43' || filtro.properties.PISOS_16==='44' || filtro.properties.PISOS_16==='45' || filtro.properties.PISOS_16==='46' || filtro.properties.PISOS_16==='47' || filtro.properties.PISOS_16==='48' || filtro.properties.PISOS_16==='49' || filtro.properties.PISOS_16==='50');

    // variable que filtra y almacena todos los edificios que sean de tipo vivienda unica y viviendaa
    const viviendas= totaledif.filter(filtro2=>filtro2.properties.TIPO2_16==='VIVIENDA' || filtro2.properties.TIPO2_16==='VU - VIVIENDA UNICA');

    //console.log(viviendas);
    
    return (
        <Fragment>
            <label>
                <img className="img-ico" src={casa} alt="icono marker"/>
                Construcciones 10+ <input type="checkbox" defaultChecked={mostrar} onClick={()=>mostrarOcultar()}/>
            </label>
            <p className="p-cantidad">Total: {totaledif.length}</p>
            
            <label>
                <img className="img-ico" src={viviendaico} alt="icono marker"/>
                Edificios(viviendas) 10+ <input type="checkbox" defaultChecked={mostrar2} onClick={()=>mostrarOcultar2()}/>
            </label>
            <p className="p-cantidad">Total: {viviendas.length}</p>

             {mostrar? totaledif.map(edificio=>(
                 <Marker
                    icon={edi10marker}
                    key={edificio.properties.id}
                    onClick={()=>setPopUp(edificio)}
                    position={[edificio.geometry.coordinates[1],
                                edificio.geometry.coordinates[0]]}
                 />
             )) :null}

             {mostrar2? viviendas.map(vivienda=>(
                 <Marker
                    icon={viviendamarker}
                    key={vivienda.properties.id}
                    onClick={()=>setPopUp(vivienda)}
                    position={[vivienda.geometry.coordinates[1],
                                vivienda.geometry.coordinates[0]]}
                 />
             )) :null}

            {popup && 
                <Popup
                    onClose={()=>setPopUp(null)}
                    position={[popup.geometry.coordinates[1],
                                popup.geometry.coordinates[0]]}
                >
                        <p><strong>Nro pisos:</strong> {popup.properties.PISOS_16}</p>
                        
                </Popup>
            }
        </Fragment>
    );
}
 
export default Edificios;