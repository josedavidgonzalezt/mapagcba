import React,{Fragment,useState} from 'react';
import {Popup,Marker} from 'react-leaflet';
import {Icon} from 'leaflet';
import * as edificios from '../../data/edificiospublicosgcba.json';
import markeredificios from './imgMarkers/estrella.png'

const EdificiosGcba = () => {
    //state para los edificios
    const [verdadero, setVerdadero]=useState(false);

    //state para mostrar y ocultar el pop up
    const [popup, guardarPopUp]=useState(null);

    //funcion que oculta y muestra los edificios
    const handleCheckEdificio=()=>{
        if(verdadero){
            setVerdadero(false)
        }
        else if(!verdadero){
            setVerdadero(true)
        }
    }
    
    //icono marker de los edificios
    const edificiosgcba= new Icon({
        iconUrl: require('./imgMarkers/estrella.png'),
        iconSize:35
    })

    return (
        <Fragment>
            <label>
                <img className="img-ico" src={markeredificios} alt="icono marker"/>
                Edificios GCBA <input 
                                    type="checkbox" 
                                    defaultChecked={verdadero} 
                                    onClick={()=>handleCheckEdificio()}/>
            </label>
            <p className="p-cantidad">Total: {edificios.features.length}</p>
            {verdadero
            
            ?  edificios.features.map(edificio=>(
                    <Marker
                        icon={edificiosgcba}
                        key={edificio.properties.Id}
                        position={[edificio.geometry.coordinates[1],edificio.geometry.coordinates[0]]}
                        onclick={()=>guardarPopUp(edificio)}
                    />
                ))
    
            :null}

            { popup &&
                    <Popup
                        position={[popup.geometry.coordinates[1]
                                    ,popup.geometry.coordinates[0]]}
                        onClose={()=>guardarPopUp(null)}
                    >
                        <div className="mypopup">
                            <p>Nombre: {popup.properties.Nombre}</p>
                        
                    </div>
                    </Popup>
            }
        </Fragment>
     );
}
 
export default EdificiosGcba;