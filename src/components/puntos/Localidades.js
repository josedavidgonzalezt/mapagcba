import React, {Fragment, useState } from 'react';
import * as barrios from '../../data/barrios.json';
import {Polygon,Tooltip} from 'react-leaflet';


const Localidades = () => {
    //state para las localidades
    const [localidades, setLocalidadees]=useState(false);

    
    //funcion que muestra y oculta las localidades
    const mostrarLocalidades =()=>{
        if (!localidades){
            setLocalidadees(true);
        }
        else if(localidades){
            setLocalidadees(false)
        }
    }

/* console.log(barrios.default.map(barrio=>barrio.geometry.coordinates[0].map(coordenadas=>coordenadas.map(esto=>esto).reverse()))) */

   
   
    return ( 
        <Fragment>
            <label>Barrios <input id="#id" type="checkbox" defaultChecked={localidades} onClick={()=>mostrarLocalidades()}/></label>
            <p className="p-cantidad">Total: {barrios.default.length}</p>
            
            {localidades ? barrios.default.map(barrio=>(
                <Polygon
                 key={barrio.properties.area}
                 positions={[barrio.geometry.coordinates[0].map(coordenadas=>coordenadas.map(esto=>esto).reverse())]}
                 opacity={.5}
                color="#000"
                >
                    <Tooltip>{barrio.properties.barrio}</Tooltip>
                </Polygon>
            )):null}
            

        </Fragment>
     );
}
 
export default Localidades;