import React,{useState} from 'react';
import PuntosVerdes from './puntos/PuntosVerdes';
import EdificiosGcba from './puntos/EdificiosGcba';
import arrow from './puntos/imgMarkers/bx-left-arrow.svg';
import arrow2 from './puntos/imgMarkers/bx-right-arrow.svg';
import obelisco from './puntos/imgMarkers/argentina.png';
import Localidades from './puntos/Localidades';
import PuntosWifi from './puntos/PuntosWifi';
import Obras from './puntos/Obras';
import CampanasVerdes from './puntos/CampanasVerdes';
import Edificios from './puntos/Edificios';

const Seleccion = () => {

    //state que actualiza el estado del menu
    const [mostrarmenu, setMostrarMenu] =useState(false);

    //funcion para mostrar y ocultar el menu
    const mostrar =()=>{
        setMostrarMenu(true);
    }
    const ocultar =()=>{
        setMostrarMenu(false);
    }

    let flechamenu;
    const imagenMenu=()=>{
        if (mostrarmenu){
            return flechamenu =<img className="icono-menu" src={arrow2} alt="icono del menu" onClick={()=>ocultar()}/>
            
        }
        else if(!mostrarmenu){
            return flechamenu= <img className="icono-menu" src={arrow} alt="icono del menu" onClick={()=>mostrar()}/>

        }
    }
    imagenMenu();

    //funcion para agregar la clase mostrar el menu
    let clase;
    const mostrarClase=()=>{
        if(mostrarmenu===true){
            clase='mostrar'
        }
        return null;
    }
    mostrarClase()
    

    return ( 
        <div className={`menu ${clase}`}>
            <div className="contenedor-titulo">
                <h2>CABA</h2>
                <img className="obelisco-img" src={obelisco} alt="icono de obelisco"/>
            </div>
            
           {flechamenu}
            <Localidades/>
            <PuntosVerdes/>
            <CampanasVerdes/>
            <EdificiosGcba/> 
            <PuntosWifi/>
            <Obras/>
            <Edificios/>
        </div>
     );
}
 
export default Seleccion;